# [SECTION] Comments
# Comments in Python are done using the "#" symbol

# [SECTION] Pyhton Syntax
print("Hello World!");

# [SECTION] Indentation
# Indentation in Python is very important
# In Python, indentation is used to indicate a block of code

# [SECTION] Variables
# The terminology used for variable names is "identifier"
# In Python, a variable is declared by stating the variable name and assigning a value using the equality symbol

# [SECTION] Naming conventions
# Python uses the snake case convention 
age = 35;
print(age);
middle_initial = "C";
print(middle_initial);

# Python allows assigning of values to multiple variables in one line
name1, name2, name3, name4 = "John", "Paul", "George", "Ringo";
print(name1);

# [SECTION] Data Types
# 1. Strings(str) - for alphanumeric and symbols
full_name = "John Doe"
print(full_name)

sercret_code = 'Pa$$word'
print(sercret_code)

# 2. Numbers(int, float, complex numbers) - for integers, decimals, and complex numbers
num_of_days = 365;
print(num_of_days);

pi_approx = 3.1416
print(pi_approx)

complex_num = 1+5j;
print(complex_num)
print(complex_num.real)
print(complex_num.imag)

# Booleans = for truth values

isLearning = True
isDifficult = False
print(isLearning)
print(isDifficult)

# [SECTION] Using Variables
# Just like in JS, variables are used by simply calling the name of the identifies
# To use variables, concatenation ("+") symbol between strings can be used
print("My name is " + full_name)
# This returns a "TypeError" as numbers can't be concatinated to strings
# print("My age is " + age)

print("My age is " + str(age))

# [SECTION] Typecasting
# There may be times when we want to specify a type on to a variable. This can be done with casting. Here are some functions that can be used:
# 1. int() - converts the value into an integer value
# 2. float() - converts the value into a float value
# 3. str() - it converts the value into strings
print(int(3.5))
print(float(3))

# Another way to avoid the ty pe error in pronting without the use of typecasting is the use of F-strings
# To use F-strings, add a lowercase "f" before the string and place the desired variable in {}
print(f"Hi, my name is {full_name} and my age is {age}")

#[SECTION] Operations
#Python has operator families that can be used to manipulate variables
#Arithmetic operators - perform mathematical operations
print(1 + 10)
print(15 - 8)
print(18 * 9)
print(21 / 7)
print(18 % 4) #remainder
print(2 ** 6) #exponent

#Assignment operators - used to assign values to variables
num1 = 3
print(num1)
num1 += 4
print(num1)
# Other assignment operators (-=, *=, /=, %=)

# Comparison operators - used to compare values (returns a boolean value)
print(1 == 1) # True
print(1 == "1") # False
# Other operators ( !=, >=, <=, >, <)

# Logical operators - used to combine conditional statements
print (True and False)
print(not False)
print(False or True)
