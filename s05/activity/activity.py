from abc import ABC, abstractmethod

class Person(ABC):
	def __init__(self, firstName, lastName, email, department):
		self._firstName = firstName
		self._lastName = lastName
		self._email = email
		self._department = department

	@abstractmethod
	def getFullName(self):
		pass

	@abstractmethod
	def addRequest(self):
		pass

	@abstractmethod
	def checkRequest(self):
		pass

	@abstractmethod
	def addUser(self):
		pass


class Employee(Person):
	def __init__(self, firstName, lastName, email, department):
		super().__init__(firstName, lastName, email, department)

	def login(self):
		return f"{self._email} has logged in"

	def logout(self):
		return f"{self._email} has logged out"

	def getFullName(self):
		return f"{self._firstName} {self._lastName}"

	def addRequest(self):
		return "Request has been added"

	def checkRequest(self):
		pass

	def addUser(self):
		pass


	# Getter and Setter
	@property
	def lastName(self):
		return self._lastName

	@lastName.setter
	def lastName(self, value):
		self._lastName = value

	@property
	def email(self):
		return self._email

	@email.setter
	def email(self, value):
		self._email = value

	@property
	def department(self):
		return self._department

	@department.setter
	def department(self, value):
		self._department = value


class TeamLead(Person):
	def __init__(self, firstName, lastName, email, department):
		super().__init__(firstName, lastName, email, department)
		self._members = []

	def addMember(self, employee):
		self._members.append(employee)
		return f"Employee {employee.getFullName()} added"

	def getFullName(self):
		return f"{self._firstName} {self._lastName}"

	def addRequest(self):
		return "Request has been added"

	def checkRequest(self):
		return "Checking request"

	def addUser(self):
		return "Adding user"

	def login(self):
		return f"{self._email} has logged in"

	def logout(self):
		return f"{self._email} has logged out"

	def get_members(self):
		return self._members


	# Getter and Setter
	@property
	def lastName(self):
		return self._lastName

	@lastName.setter
	def lastName(self, value):
		self._lastName = value

	@property
	def email(self):
		return self._email

	@email.setter
	def email(self, value):
		self._email = value

	@property
	def department(self):
		return self._department

	@department.setter
	def department(self, value):
		self._department = value

	

class Admin(Person):
	def __init__(self, firstName, lastName, email, department):
		super().__init__(firstName, lastName, email, department)

	def getFullName(self):
		return f"{self._firstName} {self._lastName}"

	def addRequest(self):
		return "Request has been added"

	def checkRequest(self):
		return "Checking request"

	def login(self):
		return f"{self._email} has logged in"

	def logout(self):
		return f"{self._email} has logged out"

	def addUser(self):
		return "User has been added"


	
	# Getter and Setter
	@property
	def lastName(self):
		return self._lastName

	@lastName.setter
	def lastName(self, value):
		self._lastName = value

	@property
	def email(self):
		return self._email

	@email.setter
	def email(self, value):
		self._email = value

	@property
	def department(self):
		return self._department

	@department.setter
	def department(self, value):
		self._department = value



class Request:
	def __init__(self, name, requester, dateRequested, status):
		self.name = name
		self.requester = requester
		self.dateRequested = dateRequested
		self.status = status

	def updateRequest(self):
		return f"Request {self.name} has been updated"

	def closeRequest(self):
		return f"Request {self.name} has been closed"

	def cancelRequest(self):
		return f"Request {self.name} has been cancelled"



if __name__ == "__main__":
	employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
	employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
	employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
	employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
	admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
	teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")

	req1 = Request("New hire orientation", teamLead1, "27-Jul-2021", "Pending")
	req2 = Request("Laptop repair", employee1, "1-Jul-2021", "Open")

	assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
	assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
	assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
	assert employee2.login() == "sjane@mail.com has logged in"
	assert employee2.addRequest() == "Request has been added"
	assert employee2.logout() == "sjane@mail.com has logged out"

	teamLead1.addMember(employee3)
	teamLead1.addMember(employee4)

	for indiv_emp in teamLead1.get_members():
		print(indiv_emp.getFullName())

	assert admin1.addUser() == "User has been added"

	req2.status = "Closed"
	print(req2.closeRequest())

	print(employee1.getFullName())
	print(employee2.getFullName())
	print(req1.closeRequest())
