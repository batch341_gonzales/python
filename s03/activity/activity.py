# S03 Activity:
# 	1. Create a Class called Camper and give it the attributes name, batch, course_type
# 	2. Create a method called career_track which will print out the string 'Currently enrolled in the <value of course_type> program'
# 	3. Create a method called info which will print out the string 'My name is <value of name> of batch <value of batch>.'
# 	4. Create an object from class Camper called zuitt_camper and pass in arguments for name, batch, and course_type
# 	5. Print the value of the object's name
# 	6. Print the value of the object's batch
# 	7. Print the value of the object's course type
# 	8. Execute the info method of the object
# 	9. Execute the career_track method of the object



class Camper:
    def __init__(self, name, batch, course_type):
        self.name = name
        self.batch = batch
        self.course_type = course_type

    def career_track(self):
        print(f"Currently enrolled in the {self.course_type} program")

    def info(self):
        print(f"My name is {self.name} of batch {self.batch}.")

# Create an object from class Camper called zuitt_camper and pass in arguments for name, batch, and course_type
zuitt_camper = Camper(name="Alan", batch=100, course_type="python short course")

# Print the values of the object's attributes
print(f"Camper Name: {zuitt_camper.name}")
print(f"Camper Batch: {zuitt_camper.batch}")
print(f"Camper Course: {zuitt_camper.course_type}")

# Execute the info and career_track methods of the object
zuitt_camper.info()
zuitt_camper.career_track()
