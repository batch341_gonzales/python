# Get a year from the user and determine if it is a leap year or not.
def is_leap_year(year):
    return (year % 4 == 0 and year % 100 != 0) or (year % 400 == 0)


# Strings are not allowed for inputs
# No zero or negative values
while True:
    try:
        year = int(input("Please input a year: "))
        if year <= 0:
            raise ValueError("Year must be a positive number.")
        break
    except ValueError as e:
        print(e)


if is_leap_year(year):
    print(f"{year} is a leap year")
else:
    print(f"{year} is not a leap year")



# Get two numbers (row and col) from the user and create by row x col grid of asterisks given the two numbers
while True:
    try:
        rows = int(input("Enter number of rows: "))
        cols = int(input("Enter number of columns: "))
        if rows <= 0 or cols <= 0:
            raise ValueError("Both rows and columns must be positive integers.")
        break
    except ValueError as e:
        print(e)

# asterisks
for _ in range(rows):
    print("*" * cols)
