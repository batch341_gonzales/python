# S04 Activity:

# 1. Create an abstract class called Animal that has the following abstract method Abstract Methods: eat(food), make_sound()
# 2. Create two classes that implements the Animal class called Cat and Dog with each of the following properties and methods:
# Properties:
# name, breed, age
# Methods:
# getters and setters, implementation of abstract methods, call()

from abc import ABC, abstractmethod

class Animal(ABC):
	@abstractmethod
	def eat(self, food):
		pass

	@abstractmethod
	def make_sound(self):
		pass


class Dog(Animal):
	def __init__(self, name, breed, age):
		self._name = name
		self._breed = breed
		self._age = age

	def eat(self, food):
		print(f"Eaten {food}")

	def make_sound(self):
		print("Bark! Woof! Arf!")

	def call(self):
		print(f"Here {self._name}!")


	# Getter and Setter
	@property
	def name(self):
		return self._name

	@name.setter
	def name(self, value):
		self._name = value

	@property
	def breed(self):
		return self._breed

	@breed.setter
	def breed(self, value):
		self._breed = value

	@property
	def age(self):
		return self._age

	@age.setter
	def age(self, value):
		self._age = value


class Cat(Animal):
	def __init__(self, name, breed, age):
		self._name = name
		self._breed = breed
		self._age = age

	def eat(self, food):
		print(f"Serve me {food}")

	def make_sound(self):
		print("Miaow! Nyaw! Nyaaaaa!")

	def call(self):
		print(f"{self._name}, come on!")


	# Getter and Setter
	@property
	def name(self):
		return self._name

	@name.setter
	def name(self, value):
		self._name = value

	# Getter and Setter for breed
	@property
	def breed(self):
		return self._breed

	@breed.setter
	def breed(self, value):
		self._breed = value

	# Getter and Setter for age
	@property
	def age(self):
		return self._age

	@age.setter
	def age(self, value):
		self._age = value



if __name__ == "__main__":
	dog = Dog("Isis", "Golden Retriever", 5)
	cat = Cat("Puss", "Siamese", 3)
	
	dog.eat("Steak")
	dog.make_sound()
	dog.call()

	cat.eat("Tuna")
	cat.make_sound()
	cat.call()